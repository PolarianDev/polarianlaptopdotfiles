# History:
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Promt
autoload -Uz promptinit
promptinit
prompt spaceship

alias rpi="ssh 192.168.0.35"

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

export PATH="$PATH:/home/polaris/.local/bin"
export PATH="/usr/share/dotnet:$PATH"
